#include "bobrand.h"
#include "salloc.h"

uint32_t bobrand(struct bobctx* x)
{
	uint32_t e;
	e = x->a - ((x->b << 27) | x->b >> (32 - 27));
	x->a = x->b ^ ((x->c << 17) | x->c >> (32 - 17));
	x->b = x->c + x->d;
	x->c = x->d + e;
	x->d = e + x->a;
	return x->d;
}

