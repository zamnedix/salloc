#pragma once
#include <stddef.h>

/***************************/
/* Allocator configuration */
/***************************/

/* This value dictates how much RAM in bytes is reserved for the allocator.
   Note that the available allocation space will be much smaller - see note
   above SALLOC_N_BLOCKS below for more details.
   
   The maximum valid value here is the number of bytes of RAM your MCU can 
   address, minus one. The AT90USB1286 in my Teensy board has 8192 bytes of 
   SRAM, so 8191 is my maximum. My application still needs some memory for 
   automatic storage (variables on the stack), so I take off 1024 bytes. */
#define SALLOC_MAX_ADDR 7167lu

/* This value dictates how many bits are used to represent an address. To
   select this value, first determine the minimum amount of bits required
   to represent the maximum address supported by your MCU. Then, round up
   to the nearest power of 2, between 8 and 32; if SALLOC_64BIT is enabled,
   8-64 is the range.
   
   For a device with 8192 bytes of SRAM, only 13 bits are needed to represent 
   an address. This means our final value for SALLOC_BITS_ADDR is 16.
   
   Rounding up ensures that each unique struct member takes up one whole type 
   as addressed by the CPU. If you don't do this, the compiler will most likely
   do it for you anyway. Any other choice would usually result in a significant
   performance impact. */ 
#define SALLOC_BITS_ADDR 16

/* This is the number of blocks allocated to the block pool. The higher this
   value is, the more objects you can allocate. The lower this value is, the
   more heap space is available for allocation. */
#define SALLOC_N_BLOCKS 128

/* If this value is nonzero, allocated sizes are rounded up to the next 
   multiple of this value. This can reduce fragmentation at the expense of
   some wasted and unused space at the end of most allocations. */
#define SALLOC_ROUND 8

/* When this value is nonzero, allocated objects will be overwritten with  
   zeroes when they are freed. */
#define SALLOC_ZEROFREE 1

/*************************/
/* Compatibility options */
/*************************/

/* If this value is nonzero, define `malloc' and `free' as macros which resolve
   to `salloc' and `sfree'. This is useful when you want to use code which
   relies on this standard convention. */
#define SALLOC_MALLOC 1

/* This value is nonzero when salloc should define its own types. This is
   needed in environments which don't provide a <stdint.h> header. */
#define SALLOC_TYPES 0

/* When this value is nonzero, value 64 is valid when used for 
   SALLOC_BITS_ADDR below. Your architecture must support 64-bit types and
   your environment must define the `unsigned long long' type. Note that this
   type is not present in strict C89/C90 environments. */
#define SALLOC_64BIT 1

/* This value is nonzero when salloc shall define its own `memset' 
   implementation. This will expose a global `memset' prototype, so don't 
   include any other `memset' implementations from any files that also include 
   this one. This is needed in environments which don't provide a
   <string.h> header. */
#define SALLOC_MEMSET 0

/* When this value is nonzero, a "usability" score is calculated during each
   allocation and free event. This score is intended to measure fragmentation,
   and represents the usable percentage of a heap */
#define SALLOC_USABILITY 1

/* This value is nonzero when salloc shall attempt to periodically "rebalance" 
   a highly-fragmented heap when the usability score is too low. During a 
   rebalance, all used objects are shifted to the left of the heap to 
   consolidate as much free space as possible. */
#define SALLOC_REBALANCE 0

/* If this value is nonzero, salloc will use its own implementation of pow().
   This is needed in environments which don't provide a <math.h> header. 
   This setting is ignored when SALLOC_USABILITY is not set. */
#define SALLOC_MATH 0

/* Define our types when needed. */
#if (SALLOC_TYPES == 1)
/* These definitions are for x86. */
typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
#if (SALLOC_64BIT)
typedef unsigned long long uint64_t;
#endif
/* Use these definitions on 8-bit AVR. */
/*
typedef unsigned char uint8_t;
typedef unsigned int uint16_t;
typedef unsigned long uint32_t;
#if (SALLOC_64BIT)
typedef unsigned long long uint64_t;
#endif
 */
#else
#include <stdint.h>
#endif

/***************************************************************************/
/* End of configuration                                                    */
/* Nothing beyond this point is intended for modification by general users */
/* of salloc                                                               */
/***************************************************************************/

/* Override malloc/free at the preprocessor level when SALLOC_MALLOC is set. We
   do things this way for two reasons:
   1. To simplify the header and interfaces. Only two actual function 
   prototypes are defined, but either convention may be used by any code using
   this library.
   2. To allow for successful compilation with Address Sanitizer (ASAN) enabled
   while developing, without tweaking extra compiler parameters. ASAN normally
   replaces malloc() with its own routine for some of its functionality. By
   doing this, we can still enjoy some of the benefits of ASAN (like 
   use-after-scope detection) while still using our own allocator.
*/
#if (SALLOC_MALLOC)
#define malloc salloc
#define free sfree
#endif

/* This calculation determines the maximum amount of space that can be 
   allocated from the heap. */
#define AVAILABLE_BYTES (SALLOC_MAX_ADDR -				\
			 (sizeof(struct salloc_block) * SALLOC_N_BLOCKS)\
			 + sizeof(struct salloc_block*))

/* Determine which type to use for virtual addresses */
#if   (SALLOC_BITS_ADDR == 8)
typedef uint8_t salloc_addr;
#elif (SALLOC_BITS_ADDR == 16)
typedef uint16_t salloc_addr;
#elif (SALLOC_BITS_ADDR == 32)
typedef uint32_t salloc_addr;
#elif (SALLOC_BITS_ADDR == 64)
#if (!SALLOC_64BIT)
#error "SALLOC_BITS_ADDR is too high - consider enabling SALLOC_64BIT"
#endif
typedef uint64_t salloc_addr;
#else
#error "Unsupported size for SALLOC_BITS_ADDR."
#endif

/* This structure represents a single region of memory. */
struct salloc_block;
struct salloc_block {
	salloc_addr start; /* Virtual address of the object */
	salloc_addr size;  /* Size of the object in bytes */
	uint8_t used;     /* Set when this object is in use */
	struct salloc_block* prev;     
	struct salloc_block* next;
}; 

/* Include the system-provided string.h when we are not using our own memset
   implementation */
#if (!SALLOC_MEMSET)
#include <string.h>
#else
/* Fill the first size bytes of the memory area pointed to by out with the 
   constant byte c */
void* memset(void* out, int c, size_t size);
#endif

/* Attempt to allocate size bytes from the salloc heap. Returns a pointer to   
   the start of the newly-allocated area if successful. */
void* salloc(size_t size);

/* Free an area previously returned by salloc. The area is zeroed and the block
   is released. */
void sfree(void* p);
