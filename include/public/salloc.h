#pragma once
#include <stddef.h>
/****** 
	This public header is intended for distribution of salloc as a shared 
	library. Internal constants and the definition of the block structure 
	are hidden from the interface. 
******/

/* Attempt to allocate size bytes from the salloc heap. Returns a pointer to   
   the start of the newly-allocated area if successful. */
void* salloc(size_t size);

/* Free an area previously returned by salloc. The area is zeroed and the block
   is released. */
void sfree(void* p);
